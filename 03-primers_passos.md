### Canvis a l'índex (_stage_ area)

Després d'afegir un fitxer a l'índex (amb l'ordre `git add`) diem que:
- al fitxer se li fa un seguiment (_tracking_).
- l'estat del fitxer és _staged_ (podríem pensar que l'estat real del fitxer al directori és el mateix que a l'índex).

Però si, sense fer cap _commit_, modifiquem un fitxer que estava a l'índex, (_tracked_ i _staged_) que passa?
- El fitxer continua sent _tracked_.
- Les versions de l'índex i la real (la del directori de treball) difereixen. El fitxer està _unstaged_.

Que podem fer?
- "Sincronitzar" amb la versió del directori de treball (la més actual) és a dir fer-lo _staged_ altre cop amb `git add el_fitxer` (opció + habitual)
- "Sincronitzar" amb la versió de l'índex (la versió més antiga, òbviament perdem el darrer canvi fet al directori de treball) amb l'ordre `git checkout -- el_fitxer`
- Treure'l de l'índex és a dir deixar de fer-li el seguiment (untracked) amb l'ordre `rm --cached el_fitxer` (opció - habitual)

![Operacions locals amb git](local_operations.png)

[Fent versions del projecte al repositori (commits)](04-primers_passos_commit.md)

