### Git com a sistema distribuit

Una de les característiques de git és que és un sistema distribuit, és a dir, podem tenir una còpia del projecte a diferents ordinadors. És molt interessant tenir una de les còpies a un ordinador que estigui al núvol: un hosting.

Primer hem de decidir:
- Quin host remot volem utilitzar
- Quin tipus d'autenticació farem servir

Nosaltres respondrem a les preguntes anteriors:
- Gitlab
- Connexió ssh

Un cop tenim configurat els ítems anteriors farem el següent:

Crearem un repositori remot de nom _origin_ que apuntarà a una adreça de _gitlab_:
  ```
  git remote add origin git@gitlab.com:pingui/intro-git.git
  ```
Per comprovar que està be:
  ```
  git remote -v
  ```
Si ens equivoquem podem esborrar:
  ```
  git remote remove origin
  ```

Ara ja podem pujar el nostre projecte local a un ordinador remot que es troba a gitlab, d'aquesta manera ja tindrem al menys dues còpies.
  ```
  git push origin master
  ```

[Com tornar a una versió anterior](06-primers_passos_revert.md)

