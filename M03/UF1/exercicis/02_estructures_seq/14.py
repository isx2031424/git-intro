#!/usr/bin/python3
#-*- coding: utf-8-*-
#diego sanchez piedra
#isx2301424
#versió 1
#Fes un programa que faci conversions de base b a base 10.
#Ha de llegir un nombre que serà la base i 3 dígits (per exemple) un a un.
#Ha de retornar el nombre en base 10.

#E.E.: 1 variable entera > 0 i 3 variables enteres >= 0

#llegir variables
base=int(input())
n1=int(input())
n2=int(input())
n3=int(input())

#fer la conversió

resultado = n1 * base**2 + n2 * base**1 + n3 * base**0

#mostrem
print(resultado)
