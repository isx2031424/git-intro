#!/usr/bin/python3
#-*- coding: utf-8-*-

#diego sanchez piedra
#isx203142
#version 1
#E.E.: 1 enter >= 0

#llegim
seconds=int(input())

hores=0

#calculem el segons restans
f_seconds = seconds % 60

#afegim als minuts

minutes = seconds // 60

#calculem el minuts restants

f_minutes = minutes % 60

#afegim a les hores

hores = minutes // 60

#mostrem
print(hores,':',f_minutes,':',f_seconds)
