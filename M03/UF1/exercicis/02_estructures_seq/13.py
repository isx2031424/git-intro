#!/usr/bin/python3
#-*- coding: utf-8-*-
#diego sanchez piedra
#isx2301424
#E.E.:8 variables entre 0 i 1
#versió 1
#programa que llegeixi 8 dígits d'una direcció binària (un dels camps d'una IP,
# per exemple) i els converteixi al seu valor decimal (volem que funcioni
#encara que sigui cutre-salsitxero).
#Ha de llegir els dígits de l'entrada un a un.

#llegim un a un
b1=int(input())
b2=int(input())
b3=int(input())
b4=int(input())
b5=int(input())
b6=int(input())
b7=int(input())
b8=int(input())

#calculem el resultat de la conversió

resultado= b1 * 2**7 + b2 * 2**6 + b3 * 2**5 + b4 * 2**4 + b5 * 2**3 + b6 * 2**2\
 + b7 * 2**1 + b8 * 2**0


#mostrem
print(resultado)
