#!/usr/bin/python3
#-*- coding: utf-8-*-
#llegir els 3 sueldos
sueldo1=int(input())
sueldo2=int(input())
sueldo3=int(input())

#constants
PERCENTATGE1=10/100
PERCENTATGE2=12/100
PERCENTATGE3=15/100

#calcular el percentatge i fer l'augment

sueldo1_final=sueldo1 * PERCENTATGE1 + sueldo1
sueldo2_final=sueldo2 * PERCENTATGE2 + sueldo2
sueldo3_final=sueldo3 * PERCENTATGE3 + sueldo3

#mostrar el resultat final

print(sueldo1_final)
print(sueldo2_final)
print(sueldo3_final)
