### Fent versions del projecte al repositori (commits)

Finalment quan considerem que ja tenim una primera versió del nostre projecte o un canvi que per a nosaltres sigui important farem un _commit_.

És obligatori escriure un missatge al _commit_ descrivint la/es característiques de la nova versió.

  ```
  git commit -m "versió que arregla la pobresa en el món"
 ```

[Git com a sistema distribuit](05-primers_passos_remot.md)
